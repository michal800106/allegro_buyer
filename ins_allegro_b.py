# vim: set et ai ts=4 sw=4 list listchars=tab\:>-,eol\:$,extends\:>,precedes\:<:
from redis import Redis
from os.path import expanduser
from statuses import bid_stats, buy_stats
import hashlib
import base64
import getpass
import argparse
from indexes import get_from_db, update_in_db, del_from_db, get_all_from_db
from datetime import datetime

def show_buy(args, db):
    if args.all:
        for buy in get_all_from_db(db, 'buy'):
            print buy
    elif args.id is not None:
        buy = get_from_db(db, 'buy', args.id)
        print buy

def show_config(args, db):
    if args.all:
        for conf in get_all_from_db(db, 'config'):
            print conf
    elif args.username is not None:
        conf = get_from_db(db, 'config', args.username)
        print conf

def del_config(args, db):
    if args.all:
        for conf in get_all_from_db(db, 'config'):
            del_from_db(db, 'config', conf['username'])
    elif args.username is not None:
        del_from_db(db, 'config', args.username)

def show_bid(args, db):
    if 'all' in args and args.all:
        for bid in get_all_from_db(db, 'bid'):
            if 'finishTime' in bid:
                bid['finishTime'] = datetime.fromtimestamp(float(bid['finishTime']))
                bid['finishTime'] = str(bid['finishTime'])
            print bid
    elif 'id' in args and args.id is not None:
        bid = get_from_db(db, 'bid', args.id)
        if 'finishTime' in bid:
            bid['finishTime'] = datetime.fromtimestamp(float(bid['finishTime']))
            bid['finishTime'] = str(bid['finishTime'])
        print bid

    elif 'buy_id' in args and args.buy_id is not None:
        for bid in get_all_from_db(db, 'bid'):
            if bid['buy_id'] == str(args.buy_id):
                if 'finishTime' in bid:
                    bid['finishTime'] = datetime.fromtimestamp(float(bid['finishTime']))
                    bid['finishTime'] = str(bid['finishTime'])
                print bid

def add_bid(args, db):
    bidbuy = get_from_db(db, 'bid', args.id)

    update_dict(bidbuy, args, 'whenShot')
    update_dict(bidbuy, args, 'price')
    update_dict(bidbuy, args, 'status')
    update_dict(bidbuy, args, 'quantity')
    update_dict(bidbuy, args, 'buy_id')
    update_dict(bidbuy, args, 'id')

    if 'whenShot' not in bidbuy:
        bidbuy['whenShot'] = -3

    if 'price' not in bidbuy:
        bidbuy['price'] = 0

    if 'status' not in bidbuy:
        bidbuy['status'] = bid_stats['NOTBIDDED']

    if 'quantity' not in bidbuy:
        bidbuy['quantity'] = 1

    update_in_db(db, 'bid', bidbuy, args.id)

def del_bid(args, db):
    bidbuy = get_from_db(db, 'bid', args.id)
    if len(bidbuy) > 0:
        del_from_db(db, 'bid', args.id)

def add_buy(args, db):
    buy = get_from_db(db, 'buy', args.id)

    update_dict(buy, args, 'id')
    update_dict(buy, args, 'desc')
    update_dict(buy, args, 'status')

    if 'status' not in buy:
        buy['status'] = buy_stats['NOTDONE']

    if 'desc' not in buy:
        buy['desc'] = 'empty desc'

    update_in_db(db, 'buy', buy, args.id)

def del_buy(args, db):
    buy = get_from_db(db, 'buy', args.id)
    if len(buy) > 0:
        del_from_db(db, 'buy', args.id)

def update_dict(dic, args, item, key=None):
    if key is None:
        key = item
    if getattr(args, item) is not None:
        dic[key] = getattr(args, item)

def config(args, db):
    conf = get_from_db(db, 'config', args.username)

    if args.password:
        password = getpass.getpass()
        hashp256 = hashlib.sha256(password)
        base_pass = base64.encodestring(hashp256.digest())
        conf['userHashPassword'] = base_pass

    update_dict(conf, args, 'username')
    update_dict(conf, args, 'countryCode')
    update_dict(conf, args, 'webapiKey')
    update_dict(conf, args, 'webapiUrl')

    if 'sessionHandlePart' not in conf:
        conf['sessionHandlePart'] = 'uu'

    if 'countryCode' not in conf:
        conf['countryCode'] = 1

    if 'localVersion' not in conf:
        conf['localVersion'] = 1

    if 'userHashPassword' not in conf:
        conf['userHashPassword'] = 'default'

    if 'webapiKey' not in conf:
        conf['webapiKey'] = 'default'

    if 'webapiUrl' not in conf:
        conf['webapiUrl'] = 'https://webapi.allegro.pl/service.php?wsdl'

    update_in_db(db, 'config', conf, args.username)

if __name__ == '__main__':
    home = expanduser("~")
    db = Redis()

    parser = argparse.ArgumentParser(prog='allegro_db')
    subparsers = parser.add_subparsers(help='sub-command --help')

    parser_add = subparsers.add_parser('add', help='add buy or bid')
    parser_del = subparsers.add_parser('del', help='del buy or bid')
    parser_show = subparsers.add_parser('show', help='show buy, config or bid')

    parser_config = subparsers.add_parser('config', help='configuration')
    parser_config.add_argument('--username', type=str, help='username for logging to allegro', required=True)
    parser_config.add_argument('--countryCode', type=str, help='country code for allegro default 1 means POLAND')
    parser_config.add_argument('--webapiKey', type=str, help='webapiKey generated from allegro.pl')
    parser_config.add_argument('--webapiUrl', type=str, help='webapiUrl for allegro.pl')
    parser_config.add_argument('--password', action='store_true', help='will ask you about password to allegro.pl')
    parser_config.set_defaults(func=config)

    subparsers_add = parser_add.add_subparsers(help='add')
    subparsers_del = parser_del.add_subparsers(help='del')
    subparsers_show = parser_show.add_subparsers(help='show')

    parser_add_buy = subparsers_add.add_parser('buy', help='add buy')
    parser_add_buy.add_argument('--id', type=int, help='id of buy', required=True)
    parser_add_buy.add_argument('--desc', type=str, help='your description of buy')
    parser_add_buy.add_argument('--status', type=str, help='"done", "bidded", "notdone"')
    parser_add_buy.set_defaults(func=add_buy)

    parser_del_buy = subparsers_del.add_parser('buy', help='del buy')
    parser_del_buy.add_argument('--id', type=int, help='id of buy', required=True)
    parser_del_buy.set_defaults(func=del_buy)

    parser_add_bid= subparsers_add.add_parser('bid', help='add bid')
    parser_add_bid.add_argument('--id', type=int, help='id of bid from webpage allegro.pl', required=True)
    parser_add_bid.add_argument('--buy_id', type=int, help='id of buy', required=True)
    parser_add_bid.add_argument('--price', type=float, help='default 0')
    parser_add_bid.add_argument('--quantity', type=int, help='default -1')
    parser_add_bid.add_argument('--whenShot', type=int, help='when bid for example -3 means that program will bid 3 seconds before end; default -3')
    parser_add_bid.add_argument('--status', type=str, help='"notbidded", "finished", "bidded"')
    parser_add_bid.set_defaults(func=add_bid)

    parser_del_bid= subparsers_del.add_parser('bid', help='del bid')
    parser_del_bid.add_argument('--id', type=int, help='id of bid', required=True)
    parser_del_bid.set_defaults(func=del_bid)

    parser_show_buy = subparsers_show.add_parser('buy', help='show buy')
    parser_show_buy.add_argument('--all', action='store_true', help='show all')
    parser_show_buy.add_argument('--id', type=int, help='id of buy')
    parser_show_buy.set_defaults(func=show_buy)

    parser_show_bid= subparsers_show.add_parser('bid', help='show bid')
    parser_show_bid.add_argument('--all', action='store_true', help='show all')
    parser_show_bid.add_argument('--id', type=int, help='id of bid')
    parser_show_bid.add_argument('--buy_id', type=int, help='id of buy')
    parser_show_bid.set_defaults(func=show_bid)

    parser_show_config= subparsers_show.add_parser('config', help='show config')
    parser_show_config.add_argument('--all', action='store_true', help='show all')
    parser_show_config.add_argument('--username', type=str, help='username')
    parser_show_config.set_defaults(func=show_config)

    parser_del_config= subparsers_del.add_parser('config', help='del config')
    parser_del_config.add_argument('--all', action='store_true', help='del all')
    parser_del_config.add_argument('--username', type=str, help='username')
    parser_del_config.set_defaults(func=del_config)

    args = parser.parse_args()
    args.func(args, db)

