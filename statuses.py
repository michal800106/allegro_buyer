bid_stats = dict(
    BIDDED="bidded"
    , NOTBIDDED="notbidded"
    , WINNING="winning"
    , WON="won"
    , FINISHED="finished"
    , NOTWINNING="notwinning"
    , NOTWON="notwon")

buy_stats = dict(
    NOTDONE="notdone"
    , BIDDED="bidded"
    , DONE="done"
    , WINNING="winning"
    , NOTWINNING="notwinning"
    , WON="won"
    , NOTWON="notwon")

config_stats = dict(
    INITIALIZED="initialized"
    , NOTINITIALIZED="notinitialized")
