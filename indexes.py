def get_from_db(db, group, key):
    group = str(group)
    key = str(key)
    res = db.hget(group, key)
    if res is None:
        return {}
    return db.hgetall(res)

def get_all_from_db(db, group):
    group = str(group)
    for key in db.hvals(group):
        yield db.hgetall(key)

def update_in_db(db, group, dicto, key):
    if len(dicto) == 0:
        return
    group = str(group)
    key = str(key)
    key_nested = ':'.join([group, key])
    db.hset(group, key, key_nested)
    db.hmset(key_nested, dicto)

def del_from_db(db, group, key):
    group = str(group)
    key = str(key)
    key_nested = ':'.join([group, key])
    db.hdel(group, key)
    db.delete(key_nested)

