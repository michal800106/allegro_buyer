# vim: set et ai ts=4 sw=4 list listchars=tab\:>-,eol\:$,extends\:>,precedes\:<:
from suds.client import Client
import suds_requests
from twisted.internet import task
from twisted.internet import reactor
from statuses import bid_stats, buy_stats, config_stats
from datetime import datetime, timedelta
from os.path import expanduser
import threading
import time
import logging
import argparse
from threading import RLock
import os
import json
import logging.config
from contextlib import contextmanager
import logging.handlers
from indexes import get_from_db, update_in_db, del_from_db, get_all_from_db
from redis import Redis
import traceback


@contextmanager
def log_and_ignored(*exceptions):
    try:
        yield
    except exceptions, e:
        err = traceback.format_exc()
        logging.error(err)

def getConfig(data):
    with log_and_ignored(Exception):
        db = data['db']
        username = data['username']
        config = get_from_db(db, 'config', username)
        config_locker = data['config_locker']
        client = data['client']
        g = client.service.doQueryAllSysStatus(config['countryCode'], config['webapiKey'])
        ver = [x['verKey'] for x in g[0] if x['countryId'] == 1][0]
        with config_locker:
            update_in_db(db, 'config', dict(localVersion=str(ver)), username)
        logRes = client.service.doLoginEnc(userLogin=config['username'],
                                           userHashPassword=config['userHashPassword'],
                                           countryCode=config['countryCode'],
                                           localVersion=config['localVersion'],
                                           webapiKey=config['webapiKey'])
        logRes_u = {}
        logRes_u['sessionHandlePart'] = str(logRes['sessionHandlePart'])
        logRes_u['userId'] = str(logRes['userId'])
        with config_locker:
            update_in_db(db, 'config', logRes_u, username)

def runEvery45Minute(data):
    logging.info("check config and relogin for new session")
    reactor.callInThread(getConfig, data)

def nowToEpoch():
    now = datetime.now()
    b = datetime.fromtimestamp(0)
    return (now - b).total_seconds()

def getShiftTime(data):
    with log_and_ignored(Exception):
        db = data['db']
        username = data['username']
        config = get_from_db(db, 'config', username)
        client = data['client']
        config_locker = data['config_locker']
        timeRes = client.service.doGetSystemTime(countryId=config['countryCode'], webapiKey=config['webapiKey'])
        with config_locker:
            config_u = {}
            config_u['shiftTime'] = timeRes - nowToEpoch()
            config_u['status'] = config_stats['INITIALIZED']
            update_in_db(db, 'config', config_u, username)
        logging.debug("serwer time: %s" % (datetime.fromtimestamp(timeRes)))

def runEveryQMinute(data):
    logging.debug("recheck shift Time")
    reactor.callInThread(getShiftTime, data)

def checkWinning(data, bid_o):
    with log_and_ignored(Exception):
        bid_id = bid_o['id']
        username = data['username']
        config = get_from_db(db, 'config', username)
        client = data['client']
        myId = config['userId']

        myId = int(myId)
        sessionId=config['sessionHandlePart']
        items = client.factory.create("ArrayOfLong")
        items.item = [bid_id]
        my_bids = client.service.doGetMyBidItems(sessionId=sessionId, itemIds=items)
        len_my_bids = my_bids['bidItemsCounter']
        if len_my_bids > 0:
            my_bids = my_bids['bidItemsList']['item']
            bid = my_bids[0]
            winnerId = bid['itemHighestBidder']['userId']
            logging.info('winnerId (%d) myId (%d)' % (winnerId, myId))
            if myId == winnerId:
                return bid_stats['WINNING']
            else:
                prices = ['itemPrice']['item']
                my_max_price = [x for x in prices if x['priceType'] == 5]
                highest_price = [x for x in prices if x['priceType'] == 2]
                if highest_price < my_max_price:
                    return bid_stats['WINNING']
                return bid_stats['NOTWINNING']

        my_not_wons = client.service.doGetMyNotWonItems(sessionId=sessionId, itemIds=items)
        len_my_not_wons = my_not_wons['notWonItemsCounter']
        if len_my_not_wons > 0:
            return bid_stats['NOTWON']
        
        my_wons = client.service.doGetMyWonItems(sessionId=sessionId, itemIds=items)
        len_my_wons = my_wons['wonItemsCounter']
        if len_my_wons > 0:
            return bid_stats['WON']

        return bid_stats['NOTWON']
    

def checkBids(data, buy, waiter):
    db = data['db']
    buy_u = {}
    for bid in get_all_from_db(db, 'bid'):
        if bid['status'] == bid_stats['BIDDED'] and bid['buy_id'] == buy['id']:
            bid_u = {}
            bid_id = bid['id']
            try:
                ret = checkWinning(data, bid)
                logging.debug('checkBids ret(%s) %s' % (ret, bid))
                if ret == bid_stats['NOTWON']:
                    buy_u['status'] = buy_stats['NOTDONE']
                    bid_u['status'] = bid_stats['FINISHED']
                if ret == bid_stats['WON']:
                    buy_u['status'] = buy_stats['DONE']
                    bid_u['status'] = bid_stats['FINISHED']
                    update_in_db(db, 'bid', bid_u, bid_id)
                    break
            except Exception, e:
                logging.error("error %s" % (e.message.encode('utf-8')))
            update_in_db(db, 'bid', bid_u, bid_id)
    update_in_db(db, 'buy', buy_u, buy['id'])
    waiter.set()

def getInfos(bid_id, config, client):
    with log_and_ignored(Exception):
        session = config['sessionHandlePart']
        items = client.factory.create("ArrayOfLong")
        items.item = [bid_id]
        try:
            res = client.service.doGetItemsInfo(sessionHandle=session, itemsIdArray=items)
            itemInfo = res['arrayItemListInfo']['item'][0]['itemInfo']
            finishTime = itemInfo['itEndingTime']
            name = itemInfo['itName']
            buy_now = itemInfo['itBuyNowActive']
            return (finishTime, name, buy_now)
        except Exception, e:
            err = traceback.format_exc()
            logging.error(err)
            logging.error("error %s" % (e.message.encode('utf-8')))
        return (None, None, None)

def checkInfo(data, buy):
    with log_and_ignored(Exception):
        db = data['db']
        username = data['username']
        config = get_from_db(db, 'config', username)
        client = data['client']
        for bid in get_all_from_db(db, 'bid'):
            if bid['buy_id'] == buy['id']:
                session = config['sessionHandlePart']
                whenShot = round(float(bid['whenShot']))
                if bid.get('buy_now', None) is None or bid.get('finishTime', None) is None or bid.get('name', None) is None or whenShot >= 0:
                    (finishTime, name, buy_now) = getInfos(bid['id'], config, client)
                    bid_u = {}
                    if finishTime is not None:
                        if whenShot >= 0:
                            serverNow = nowToEpoch() + float(config['shiftTime'])
                            bid_u['whenShot'] = round(serverNow - int(finishTime) + whenShot)
                        bid_u['finishTime'] = str(finishTime)
                    if name is not None:
                        bid_u['name'] = name
                    if buy_now is not None:
                        bid_u['buy_now'] = buy_now
                    update_in_db(db, 'bid', bid_u, bid['id'])

def doBids(data, buy, waiter):
    with log_and_ignored(Exception):
        buy_u = {} 
        db = data['db']
        username = data['username']
        config = get_from_db(db, 'config', username)
        client = data['client']
        for bid in get_all_from_db(db, 'bid'):
            if bid['status'] == bid_stats['NOTBIDDED'] and bid['buy_id'] == buy['id']:
                bid_u = {}
                serverNow = nowToEpoch() + float(config['shiftTime'])
                session = config['sessionHandlePart']
                logging.debug(bid)
                logging.debug("calculated time: %s" % (datetime.fromtimestamp(serverNow)))

                whenShot = bid['whenShot']
                whenShot = float(whenShot)
                whenShot = round(whenShot)
                
                price = bid['price']
                price = float(price)
                quantity = bid['quantity']
                quantity = int(quantity)
                bid_id = bid['id']

                finishTime = bid.get('finishTime', None)
                if finishTime is None:
                    continue

                finishTime = int(finishTime)
                if serverNow > finishTime + whenShot and serverNow < finishTime + 5:
                    buy_u['status'] = buy_stats['BIDDED']
                    bid_u['status'] = bid_stats['BIDDED']
                    try:
                        logging.info("bidding %s" % (bid_id,))
                        client.service.doBidItem(sessionHandle=session,
                                                 bidItId=bid_id,
                                                 bidUserPrice=price,
                                                 bidQuantity=quantity)
                        ret = checkWinning(data, bid)
                        logging.info('doBids ret(%s) %s' % (ret, bid))
                        if ret == bid_stats['NOTWON']:
                            buy_u['status'] = buy_stats['NOTDONE']
                            bid_u['status'] = bid_stats['FINISHED']
                        if ret == bid_stats['WON']:
                            buy_u['status'] = buy_stats['DONE']
                            bid_u['status'] = bid_stats['FINISHED']
                            update_in_db(db, 'bid', bid_u, bid_id)
                            break
                        if ret == bid_stats['WINNING']:
                            bid_u['status'] = bid_stats['BIDDED']
                            update_in_db(db, 'bid', bid_u, bid_id)
                            break
                    except Exception, e:
                        logging.error("error %s" % (e.message.encode('utf-8')))
                update_in_db(db, 'bid', bid_u, bid_id)
        update_in_db(db, 'buy', buy_u, buy['id'])
        waiter.set()

def runEverySecond(data):
    with log_and_ignored(Exception):
        logging.debug("resS")
        db = data['db']
        username = data['username']
        config = get_from_db(db, 'config', username)
        client = data['client']
        waiters = data['waiters']
        waiters_bids = data['waiters_bids']

        #logging.debug(client.service.doGetMyWonItems(sessionId=config['sessionHandlePart']))
        for buy in get_all_from_db(db, 'buy'):
            if config['status'] == config_stats['NOTINITIALIZED']:
                break

            if buy['status'] == buy_stats['NOTDONE']:
                buy_id = buy['id']
                logging.debug(buy)
                waiter = waiters.get(buy_id, None)
                if waiter is None or waiter.isSet():
                    logging.debug("we can go")
                    logging.debug(waiter)
                    waiter = threading.Event()
                    waiters[buy_id] = waiter
                    reactor.callInThread(doBids, data, buy, waiter)
                else:
                    logging.debug("we must wait")
            elif buy['status'] == buy_stats['BIDDED']:
                buy_id = buy['id']
                logging.debug(buy)
                waiter = waiters_bids.get(buy_id, None)
                if waiter is None or waiter.isSet():
                    logging.debug("we can go")
                    logging.debug(waiter)
                    waiter = threading.Event()
                    waiters_bids[buy_id] = waiter
                    reactor.callInThread(checkBids, data, buy, waiter)
                else:
                    logging.debug("we must wait")
            reactor.callInThread(checkInfo, data, buy)

if __name__ == '__main__':
    home = expanduser("~")
    db = Redis()
    
    parser = argparse.ArgumentParser(prog='allegro_b')
    parser.add_argument('--username', type=str, help='desc help', required=True)
    parser.add_argument('--verbose', action='store_true', help='verbose logging')
    args = parser.parse_args()

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    config_initial = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'detailed': {
                'class': 'logging.Formatter',
                'format': '%(asctime)s %(name)-15s %(levelname)-8s %(processName)-10s %(message)s'
            },
            'simple': {
                'class': 'logging.Formatter',
                'format': '%(name)-15s %(levelname)-8s %(processName)-10s %(message)s'
            }
        },
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'level': level,
                'formatter': 'simple',
            },
            'file': {
                'class': 'logging.FileHandler',
                'filename': 'mplog.log',
                'mode': 'w',
                'formatter': 'detailed',
            },
            'foofile': {
                'class': 'logging.FileHandler',
                'filename': 'mplog-foo.log',
                'mode': 'w',
                'formatter': 'detailed',
            },
            'errors': {
                'class': 'logging.FileHandler',
                'filename': 'mplog-errors.log',
                'mode': 'w',
                'level': 'ERROR',
                'formatter': 'detailed',
            },
        },
        'root': {
            'level': level,
            'handlers': ['console', 'file', 'errors']
        },
    }

    logging.config.dictConfig(config_initial)

    update_in_db(db, 'config', dict(status=config_stats['NOTINITIALIZED']), args.username)
    config = get_from_db(db, 'config', args.username)

    client = Client(config['webapiUrl'], transport=suds_requests.RequestsTransport())
    config_locker = RLock()
    waiters = {}
    waiters_bids = {}
    data = {'db':db, 'username':args.username, 'client':client, 'waiters': waiters, 'waiters_bids':waiters_bids, 'config_locker':config_locker}
    l = task.LoopingCall(runEverySecond, data)
    l.start(1.0) # call every second
    
    l = task.LoopingCall(runEveryQMinute, data)
    l.start(15.0) # call every 15 seconds
    
    l = task.LoopingCall(runEvery45Minute, data)
    l.start(45*60) # call every 45 minutes
    
    # l.stop() will stop the looping calls
    reactor.run()
